#!/bin/bash

# Debian releases:
#   buster=10 (2019-06), bullseye=11 (2021-08), bookworm=12 (2023-06),
#   trixie=13, forky=14
# Ubuntu releases:
#   focal=20.04 LTS, groovy=20.10, hirsute=21.04, impish=21.10
#   jammy=22.04 LTS, kinetic=22.10, lunar=23.04, mantic=23.10

set -eu

SUPPORTED_ARCHITECTURES="amd64 arm64"
SUPPORTED_DESKTOPS="default gnome kde xfce"
SUPPORTED_DISTROS="debian kali ubuntu"

# Sensible defaults for a desktop image
DEFAULT_LOCALE=en_US.UTF-8
DEFAULT_TIMEZONE=US/Eastern

ARCH=amd64
COMPONENTS=
DISTRO=debian
DESKTOP=
DESKTOP_PKG=
MIRROR=
PACKAGES=
SSH=
SUITE=
SIZE=10GiB

LOCALE=
PASSWORD=
TIMEZONE=
USERNAME=
USERPASS=

DEFAULT_DEBIAN_COMPONENTS=main
DEFAULT_DEBIAN_MIRROR=http://deb.debian.org/debian
DEFAULT_DEBIAN_SUITE=sid

DEFAULT_KALI_COMPONENTS=main,contrib,non-free
DEFAULT_KALI_MIRROR=http://http.kali.org/kali
DEFAULT_KALI_SUITE=kali-rolling

DEFAULT_UBUNTU_COMPONENTS=main,restricted,universe,multiverse
DEFAULT_UBUNTU_MIRROR=http://archive.ubuntu.com/ubuntu
DEFAULT_UBUNTU_SUITE=lunar

WELL_KNOWN_CACHING_PROXIES="\
3142 apt-cacher-ng
8000 squid-deb-proxy"
DETECTED_CACHING_PROXY=

get_locale() { [ -v $LANG ] && echo $LANG || echo $DEFAULT_LOCALE; }
get_timezone() { [ -h /etc/localtime ] \
    && realpath --relative-to /usr/share/zoneinfo /etc/localtime \
    || echo $DEFAULT_TIMEZONE; }

b() { tput bold; echo -n "$@"; tput sgr0; }
i() { tput sitm; echo -n "$@"; tput sgr0; }
fail() { echo "$@" >&2; exit 1; }

fail_invalid() {
    local msg="Invalid value '$2' for option $1"
    shift 2; [ $# -gt 0 ] && msg="$msg ($@)"
    fail "$msg"
}

in_list() {
    local word=$1 && shift
    local item=
    for item in "$@"; do
        [ "$item" = "$word" ] && return 0
    done
    return 1
}

[ $(id -u) -eq 0 ] && fail "No need to be root. Please run as normal user."

USAGE="Usage: $(basename $0) [ARGS...]

Build a minimal Debian (or derivative) OS image.

By default, build a $(b $ARCH ${DISTRO^}) image of size $(b $SIZE),
using the suite $(b $DEFAULT_DEBIAN_SUITE) and the mirror $(b $DEFAULT_DEBIAN_MIRROR).
Do not install any desktop environment.

Options to build the system:
* -a ARCHITECTURE  among: $(b $SUPPORTED_ARCHITECTURES)
* -c COMPONENTS    among: $(i depends on distro)
* -d DISTRIBUTION  among: $(b $SUPPORTED_DISTROS)
* -m MIRROR
* -s SUITE         among: $(i depends on distro)
* -z SIZE          disk size, eg. \"10GiB\"

Options to customize the system:
* -D DESKTOP       install a desktop environment, among: $(b $SUPPORTED_DESKTOPS)
* -L LOCALE        set locale
* -P PACKAGES      install extra packages (comma/space separated list)
* -S               install and enable SSH server
* -T TIMEZONE      set timezone
* -U USERPASS      create unprivileged user (of the form <username>:<password>)

For server images, the following defaults are applied:
* SSH server is always installed, LOCALE: en_US.UTF-8, TIMEZONE: Etc/UTC

For desktop images, the following defaults are applied:
* USERPASS: $USER:$USER, LOCALE: $(get_locale), TIMEZONE: $(get_timezone)
"

while getopts ":a:c:d:D:hL:m:P:s:S:T:U:z:" opt; do
    case $opt in
	(a) ARCH=$OPTARG ;;
        (c) COMPONENTS=$OPTARG ;;
        (d) DISTRO=$OPTARG ;;
        (D) DESKTOP=$OPTARG ;;
	(h) echo "$USAGE" && exit 0 ;;
        (m) MIRROR=$OPTARG ;;
        (L) LOCALE=$OPTARG ;;
        (P) PACKAGES="$PACKAGES $OPTARG" ;;
	(s) SUITE=$OPTARG ;;
        (S) SSH=1 ;;
        (T) TIMEZONE=$OPTARG ;;
        (U) USERPASS=$OPTARG ;;
        (z) SIZE=$OPTARG ;;
	(*) fail "$USAGE" ;;
    esac
done
shift $((OPTIND - 1))

# Validate architecture
in_list $ARCH $SUPPORTED_ARCHITECTURES || fail_invalid -a $ARCH

# Validate distro, set default values accordingly
case $DISTRO in
    (debian)
        [ "$COMPONENTS" ] || COMPONENTS=$DEFAULT_DEBIAN_COMPONENTS
        [ "$MIRROR" ] || MIRROR=$DEFAULT_DEBIAN_MIRROR
        [ "$SUITE"  ] || SUITE=$DEFAULT_DEBIAN_SUITE
        ;;
    (kali)
        [ "$COMPONENTS" ] || COMPONENTS=$DEFAULT_KALI_COMPONENTS
        [ "$MIRROR" ] || MIRROR=$DEFAULT_KALI_MIRROR
        [ "$SUITE"  ] || SUITE=$DEFAULT_KALI_SUITE
        ;;
    (ubuntu)
        [ "$COMPONENTS" ] || COMPONENTS=$DEFAULT_UBUNTU_COMPONENTS
        [ "$MIRROR" ] || MIRROR=$DEFAULT_UBUNTU_MIRROR
        [ "$SUITE"  ] || SUITE=$DEFAULT_UBUNTU_SUITE
        ;;
    (*)
        fail_invalid -d $DISTRO
        ;;
esac

COMPONENTS=$(echo $COMPONENTS | sed "s/[, ]\+/, /g")

# Configuration for desktop images
if [ "$DESKTOP" ]; then
    # Validate some options
    in_list $DESKTOP $SUPPORTED_DESKTOPS || fail_invalid -D $DESKTOP
    # Set sensible defaults according to the host
    [ "$LOCALE"   ] || LOCALE=$(get_locale)
    [ "$TIMEZONE" ] || TIMEZONE=$(get_timezone)
    [ "$USERPASS" ] || USERPASS=$USER:$USER
    # Work out the name for the desktop metapackage
    case $DISTRO in
        (debian)
            [ "$DESKTOP" = default ] && DESKTOP=gnome
            DESKTOP_PKG=task-$DESKTOP-desktop
            ;;
        (kali)
            [ "$DESKTOP" = default ] && DESKTOP=xfce
            DESKTOP_PKG=kali-desktop-$DESKTOP
            ;;
        (ubuntu)
            case $DESKTOP in
                (default|gnome) DESKTOP_PKG=ubuntu-desktop-minimal ;;
                (kde)  DESKTOP_PKG=kubuntu-desktop ;;
                (xfce) DESKTOP_PKG=xubuntu-desktop ;;
            esac
            ;;
    esac
else
    # Install SSH server
    SSH=1
    # Set sensible defaults
    [ "$LOCALE"   ] || LOCALE=en_US.UTF-8
    [ "$TIMEZONE" ] || TIMEZONE=Etc/UTC
fi

# Unpack USERPASS to USERNAME and PASSWORD
if [ "$USERPASS" ]; then
    echo $USERPASS | grep -q ":" \
        || fail "Invalid value for -U, must be of the form '<username>:<password>'"
    USERNAME=$(echo $USERPASS | cut -d: -f1)
    PASSWORD=$(echo $USERPASS | cut -d: -f2-)
    unset USERPASS
fi

# Order packages alphabetically, separate each package with ", "
PACKAGES=$(echo $PACKAGES | sed "s/[, ]\+/\n/g" | LC_ALL=C sort -u \
    | awk 'ORS=", "' | sed "s/[, ]*$//")

# Attempt to detect well-known http caching proxies on localhost,
# cf. bash(1) section "REDIRECTION". This is not bullet-proof.
if ! [ -v http_proxy ]; then
    while read port proxy; do
        (</dev/tcp/localhost/$port) 2>/dev/null || continue
        DETECTED_CACHING_PROXY="$port $proxy"
        export http_proxy="http://10.0.2.2:$port"
        break
    done <<< "$WELL_KNOWN_CACHING_PROXIES"
fi

# Print a summary
echo "# Proxy configuration:"
if [ "$DETECTED_CACHING_PROXY" ]; then
    read port proxy <<< $DETECTED_CACHING_PROXY
    echo "Detected caching proxy $(b $proxy) on port $(b $port)."
fi
if [ "${http_proxy:-}" ]; then
    echo "Using proxy via environment variable: $(b http_proxy=$http_proxy)."
else
    echo "No http proxy configured, all packages will be downloaded from Internet."
fi

echo "# Build options:"
echo "Build a $(b ${DISTRO^}) OS image for the $(b $ARCH) architecture. Disk size: $(b $SIZE)."
echo "Get packages from the $(b $SUITE) suite and $(b $COMPONENTS) components."
echo "Fetch packages from mirror $(b $MIRROR)."
echo "Configure the system according to:"
[ "$DESKTOP"  ] && echo "* install the $(b $DESKTOP) desktop environment"
[ "$PACKAGES" ] && echo "* additional packages: $(b $PACKAGES)"
[ "$LOCALE"   ] && echo "* locale: $(b $LOCALE)"
[ "$TIMEZONE" ] && echo "* timezone: $(b $TIMEZONE)"
[ "$USERNAME" ] && echo "* username: $(b $USERNAME), password: $(b $PASSWORD)"
read -p "Ok? (Hit <Ctrl+C> to cancel) "

# Work out artifact names
case $DISTRO in
    (kali)
        ROOTFS=$SUITE-$ARCH.tar.gz
        IMAGE=$SUITE-$ARCH.img
        ;;
    (*)
        ROOTFS=$DISTRO-$SUITE-$ARCH.tar.gz
        IMAGE=$DISTRO-$SUITE-$ARCH.img
        ;;
esac

[ "$DESKTOP" ] && OPTS="--scratchsize=10G" || OPTS=""

if ! [ -e $ROOTFS ]; then
    debos -t arch:$ARCH -t distro:$DISTRO -t suite:$SUITE -t components:"$COMPONENTS" -t mirror:$MIRROR \
        -t desktop:$DESKTOP -t desktoppkg:$DESKTOP_PKG -t packages:"$PACKAGES" -t ssh:$SSH \
        -t locale:$LOCALE -t timezone:$TIMEZONE \
        -t username:$USERNAME -t password:"$PASSWORD" \
        -t rootfs:$ROOTFS $OPTS rootfs.yaml
else
    echo "Re-using the existing rootfs: $ROOTFS."
    read -p "Ok? (Hit <Ctrl+C> to cancel) "
fi

debos -t arch:$ARCH -t distro:$DISTRO -t suite:$SUITE -t size:$SIZE \
    -t rootfs:$ROOTFS -t image:$IMAGE $OPTS image.yaml    # -t etcher:yes
