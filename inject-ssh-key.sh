#!/bin/bash

set -eu

IMAGE=
SSH_KEY=

USAGE="Usage: $(basename $0) IMAGE SSH_PUBLIC_KEY

Inject a SSH public key in the image, to login as root.
"

fail() { echo "$@" >&2; exit 1; }

check_file() {
    [ -e "$1" ] || fail "File '$1' does not exist"
    [ "${1##*.}" = "$2" ] || fail "File '$1' has wrong extension, should be '.$2'"
}

[ $# -eq 2 ] || fail "$USAGE"
IMAGE=$1 && check_file "$IMAGE" img
SSH_KEY=$2 && check_file "$SSH_KEY" pub

[ $(id -u) -eq 0 ] || exec sudo "$0" "$@"

setup() {
    loopdev=$(losetup --show -fP "$IMAGE")
    mntdir=$(mktemp -d $(pwd)/mnt.XXXXXX)
    mount -v ${loopdev}p2 $mntdir
}

cleanup() {
    if [ -v mntdir ] && [ "$mntdir" ]; then
        if mountpoint -q $mntdir; then
            sync -f $mntdir
            while true; do
                umount -v $mntdir && break
                echo "retrying in a sec..."
                sleep 1
            done
        fi
        rmdir $mntdir
    fi
    if [ -v loopdev ] && [ "$loopdev" ]; then
        losetup -d $loopdev
    fi
}

trap cleanup EXIT
setup
[ -d $mntdir/root/.ssh ] || mkdir -m 0700 $mntdir/root/.ssh
install -m 0600 "$SSH_KEY" $mntdir/root/.ssh/authorized_keys
# cleanup is called on exit, no need to call it
