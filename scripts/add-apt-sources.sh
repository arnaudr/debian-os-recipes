#!/bin/bash

set -eu

DEBIAN_TEMPLATE="\
deb {{ MIRROR }} {{ RELEASE }}-updates {{ COMPONENTS }}
deb http://security.debian.org/debian-security {{ RELEASE }}-security {{ COMPONENTS }}"

DEBIAN_OLD_TEMPLATE="\
deb {{ MIRROR }} {{ RELEASE }}-updates {{ COMPONENTS }}
deb http://security.debian.org/debian-security {{ RELEASE }}/updates {{ COMPONENTS }}"

UBUNTU_TEMPLATE="\
deb {{ MIRROR }} {{ RELEASE }}-updates {{ COMPONENTS }}
deb {{ MIRROR }} {{ RELEASE }}-backports {{ COMPONENTS }}
deb http://security.ubuntu.com/ubuntu {{ RELEASE }}-security {{ COMPONENTS }}"

TEMPLATE=
DISTRO=$(. /etc/os-release; echo $ID)
read -r _ MIRROR RELEASE COMPONENTS < /etc/apt/sources.list

echo "Detected: distro=$DISTRO, release=$RELEASE, components='$COMPONENTS'"
echo "Detected: mirror=$MIRROR"

in_list() {
    local word=$1 && shift
    local item=
    for item in "$@"; do
        [ "$item" = "$word" ] && return 0
    done
    return 1
}

case "$DISTRO" in
    debian)
        if in_list $RELEASE testing unstable sid; then
            true
        elif in_list $RELEASE buster stretch jessie; then
            TEMPLATE="$DEBIAN_OLD_TEMPLATE"
        else    # from bullseye onward
            TEMPLATE="$DEBIAN_TEMPLATE"
        fi
        ;;
    kali)
        true
        ;;
    ubuntu)
        TEMPLATE="$UBUNTU_TEMPLATE"
        ;;
    *)
        echo "Unsupported distribution '$DISTRO'" >&2
        exit 1
        ;;
esac

if [ -z "$TEMPLATE" ]; then
    echo "No need for extra apt sources"
    exit 0
fi

echo "Adding extra apt sources to /etc/apt/sources.list"
echo "$TEMPLATE" | sed \
    -e "s;{{ MIRROR }};$MIRROR;" \
    -e "s;{{ RELEASE }};$RELEASE;" \
    -e "s;{{ COMPONENTS }};$COMPONENTS;" \
    >> /etc/apt/sources.list

apt-get update
apt-get full-upgrade -y
