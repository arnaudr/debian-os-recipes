# Minimal Debian OS image

Debos recipes to create a minimal Debian (or Debian derivative) disk image.
The goal is to quickly build images suitable for local use. Supported
distributions are Debian, Kali and Ubuntu.

The disk image comes with a GPT partition layout, and only two partitions: the
ESP (EFI System Partition) and the root partition. `systemd-boot` is the boot
manager, no need for GRUB.

There are two main types of image: server and desktop.

To get a "desktop image" pass `-D default|gnome|kde|xfce`. In a desktop image,
by default, an unprivileged user `$USER` is created, and locale and timezone
are set according to the host system. This can be overriden with command-line
options.

If you don't pass the `-D` argument, you'll get a "server image": a SSH server
is installed, and the default locale is set to `en_US.UTF-8`.


## How to build an image

Install the required dependencies:

    sudo apt install debos

Then build images with the script `build.sh`. Start with:

    ./build.sh -h

Quick examples below.

Build a Debian unstable image ready to be managed with Ansible:

    ./build.sh -P git,python3-apt,sudo
    ./inject-ssh-key.sh debian-sid-amd64.img ~/.ssh/id_rsa.pub

Build an Debian stable image with Prometheus installed:

    ./build.sh -s bookworm -P prometheus,prometheus-node-exporter
    ./inject-ssh-key.sh debian-bookworm-amd64.img ~/.ssh/id_rsa.pub

## Caching proxy

The build script attempts to detect well-known caching proxies such as
apt-cacher-ng and squid-deb-proxy. If the detection doesn't work for you, just
export the variable `http_proxy` by yourself.  Remember that the build is done
within a QEMU VM, therefore you can't use `http://localhost` to reach your own
machine, instead use `http://10.0.2.2` or something similar. For more details,
please refer to <https://github.com/go-debos/debos#environment-variables>.


## How to run an image with QEMU

Set the image to run in a variable:

    IMG=debian-amd64.img

The simplest command to run an image:

    kvm -cpu host -m 1G -nographic \
      -drive file=/usr/share/ovmf/OVMF.fd,format=raw,if=pflash,readonly=on \
      -drive file=${IMG:?},format=raw

A bit more useful now, the command below gives you:
- fully functional EFI (ie. writable NVRAM)
- SSH access: run `ssh -p 10022 $USER@localhost` to log in the VM
- access to the host's home directory: mount it with `mount -t 9p home /mnt`

    cp -fv /usr/share/OVMF/OVMF_VARS.fd ${IMG:?}.ovmf
    kvm -cpu host -m 1G -nographic \
        -device e1000,netdev=net0 -netdev user,id=net0,hostfwd=tcp::10022-:22 \
        -virtfs local,mount_tag=home,path=$HOME \
        -drive file=/usr/share/OVMF/OVMF_CODE.fd,format=raw,if=pflash,readonly=on \
        -drive file=${IMG:?}.ovmf,format=raw,if=pflash \
        -drive file=${IMG:?},format=raw

If ever you build an image with a desktop environment, you'll want graphics and
sound, so it becomes a bit more complicated. In the command below we'll use
SPICE, so you need to run `remote-viewer` to connect to the VM.

    cp -fv /usr/share/OVMF/OVMF_VARS.fd ${IMG:?}.ovmf
    kvm -cpu host -m 4G \
        -device e1000,netdev=net0 -netdev user,id=net0,hostfwd=tcp::10022-:22 \
        -device intel-hda -device hda-duplex \
        -spice port=5930,disable-ticketing=on \
        -device virtio-serial \
        -device virtserialport,chardev=vdagent,name=com.redhat.spice.0 \
        -chardev spicevmc,debug=0,id=vdagent,name=vdagent -vga qxl \
        -virtfs local,mount_tag=home,path=$HOME \
        -drive file=/usr/share/OVMF/OVMF_CODE.fd,format=raw,if=pflash,readonly=on \
        -drive file=${IMG:?}.ovmf,format=raw,if=pflash \
        -drive file=${IMG:?},format=raw


## Known issues & limitations

Maintaining a configurable build system, for 3 different distros, and different
suites, and then also offering either server or desktop install, well, it's
actually quite some work, work that I don't do. So the most likely is that the
latest Debian suites (stable, testing, bullseye, buster) can more or less
build, Kali as well (as it's based on Debian testing), and for the rest, you'll
have to fix the build.

Other than that:

* In the Kali desktop image, the network is not functional due to the existence
  of the symlink `/etc/resolv.conf -> ../run/systemd/resolve/stub-resolv.conf`,
  while systemd-resolved is not running...  Remove the symlink and restart the
  NetworkManager service to fix it. Seems to be caused by #1007018.
